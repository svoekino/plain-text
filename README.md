# Plain Text

The browser extension "Plain text" is a notepad for quick notes.

It has 15 notes and 10 color markers.

The text is auto saved in the browser.

![Image alt](https://github.com/svoekino/plain-text-2/raw/develop/screenshots/2020-01-18-16-32-31.png)

![Image alt](https://github.com/svoekino/plain-text-2/raw/develop/screenshots/2020-01-18-16-34-56.png)

![Image alt](https://github.com/svoekino/plain-text-2/raw/develop/screenshots/2020-01-18-16-35-04.png)

![Image alt](https://github.com/svoekino/plain-text-2/raw/develop/screenshots/2020-01-18-16-35-10.png)

## Installation

- [Firefox add-on](https://addons.mozilla.org/ru/firefox/addon/plain-text)

- [Oprea or Yandex Browser extension](https://addons.opera.com/ru/extensions/details/plain-text/)

## License

GNU (GPL)

